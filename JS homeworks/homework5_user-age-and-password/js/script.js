"use strict";

function createNewUser(){

    let firstName = prompt ("Введите ваше имя:");
    let lastName = prompt ("Введите вашу фамилию:");

    let birthday = prompt ("Введите вашу дату рождения в формате `dd.mm.yyyy`:");
    let birthdayDate = new Date(birthday.replace(/^(\d+)\.(\d+).(\d+)$/, "$3-$2-$1"));
    let currentDate = new Date();
    let userAge = birthdayDate.getFullYear();
    let currentYear = currentDate.getFullYear();

    function getAge() {
        return currentYear - userAge 
    };
    console.log(getAge());

    function getPassword(){
        return firstName[0].toUpperCase() + lastName.toLowerCase() + userAge
    };
    console.log(getPassword());

    const newUser = {
        firstName,
        lastName,
        getLogin(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    };
    console.log(newUser.getLogin());
}

createNewUser();



// Экранирование это способ обозначения спец символов в строке, чтобы они не имели особого значения. Например кавычку: 'I Can\'t'; или обратный слэш: "\\";