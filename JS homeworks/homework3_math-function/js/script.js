"use strict";

let firstNumber = prompt("Укажите первое число");
while(!firstNumber || firstNumber.trim() === "" || isNaN(+firstNumber)){
    firstNumber = prompt("Укажите первое число", firstNumber);
}

let secondNumber = prompt("Укажите второе число");
while(!secondNumber || secondNumber.trim() === "" || isNaN(+secondNumber)){
    secondNumber = prompt("Укажите второе число", secondNumber);
}

let operation = prompt("Укажите математическую операцию");
while(operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/"){
    operation = prompt("Укажите математическую операцию", operation);
}

calculation(firstNumber, secondNumber, operation);

function calculation(a, b, c){
    if(operation === "+"){
        console.log(+firstNumber + +secondNumber);
    } else if(operation === "-"){
        console.log(firstNumber - secondNumber);
    } else if(operation === "*"){
        console.log(firstNumber * secondNumber);
    } else if(operation === "/"){
        console.log(firstNumber / secondNumber);
    }
}




//Теория

//1. Своими словами функции нужны для того чтобы не использовать один и тот же код в разных местах. Мы пишем функцию раз и просто вызываем её, когда нам нужно ее действие.
//2. Аргументы нужны для того чтобы функция выполнила над ними действие. Формально аргументы подставляются на место параметров и функция подставляет их значение в свое тело.