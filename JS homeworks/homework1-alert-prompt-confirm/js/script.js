"use strict";

let userName;
let userAge;

do{
    userName = prompt("Введите ваше имя:");
} while (!userName || +userName || userName.trim() === "")

do{
    userAge = prompt("Введите ваш возраст:");
} while (!userAge || userAge.trim() === "" || isNaN(+userAge))

let confirmation

if(userAge < 18){
    alert("You are not allowed to visit this website");
} else if(userAge >= 18 && userAge <= 22){
    confirmation = confirm("Are you sure you want to continue?");
    if(!confirmation){
        alert("You are not allowed to visit this website");
    } else{
        alert(`Welcome, ${userName}`)
    }
} else{
    alert(`Welcome, ` + userName)
}


// Теоретические вопросы

// 1. Первое главное отличие объявления переменных let и const от var то, что область видимости последней всегда глобальная, а первых двух может быть в рамках одного блока.

// Вторым отличием есть то, что var существует дообъявления и имеет тип данных unfedined, а let и const будут видны только после объявления.

// При переменной let в цикле, то для каждой итерации создется своя новая переменная, а переменная var одна для всех итераций цикла и видна после.

// Ну и const не может изменить свое значения после объявления, а let может менять.

// 2. Очевидной проблемой переменной var является её глобальная область видимости, через что объявлять переменную через var считается плохим тоном. Также var нагружает браузер, что можно увидеть через интерфейс window в консоли. На сколько я понял, переменные созданные через var будут отображатся через window даже если они не задействуваны, а это нам не нужно. 