"use strict";


function filterBy(array, type){

    if(!Array.isArray(array)){
        alert("Введите масив первым аргументом!");
        return;
    };
    return array.filter(element => typeof element !== type);
   
};

console.log(filterBy([1, 2, 3, 4, 5, 6, "number", "dadw", [1, 2, 3], null], "number"));



// Метод forEach() выполняет прописаную колбэк функцию для каждого елемента массива.



